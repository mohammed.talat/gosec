# Gosec analyzer changelog

## v3.5.2
- Update deps (!150)
- Update gosec to [2.11.0](https://github.com/securego/gosec/releases/tag/v2.11.0) (!150)
  - Adds directory traversal for Http.Dir("/")

## v3.5.1
- Update gosec to [2.10.0](https://github.com/securego/gosec/releases/tag/v2.10.0) (!147 @feistel)
    - Resolve the TLS min version when is declarted in the same package but in a different file

## v3.5.0
- Update ruleset, report, and command modules to support ruleset overrides (!145)

## v3.4.7
- Update report dependency to v3.7.1

## v3.4.6
- Update gosec to [2.9.6](https://github.com/securego/gosec/releases/tag/v2.9.6) (!141)

## v3.4.5
- Update gosec to [2.9.5](https://github.com/securego/gosec/releases/tag/v2.9.5) (!137)
    - chore(deps): update golang.org/x/crypto commit hash to 4570a08
    - Fix a panic in suproc rule when the declaration of the variable is not available in the AST
    - Use go embed for templates
    - Fix crash when parsing the TLS min version value
    - G303: catch with os.WriteFile, add os.Create test case
    - Spelling fixes (#717)

## v3.4.4
- Bumps post-analyzer/scripts version for more consistent error handling (!138)

## v3.4.3
- Fixed a bug that where gosec was not executed properly when ruleset config (but no passthrough config) was present. (!136)

## v3.4.2
- Log error on gosec analysis failure by default (!133)

## v3.4.1
- Update common to `v2.24.1` which fixes a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!135)

## v3.4.0
- Bump ruleset module (!134)

## v3.3.4
- Update command package to v1.5.1 (!132)
  - Report output will now be optimized
- Update common package to v2.24.0 (!132)
- Update report package to v3.6.0 (!132)
- Update to gosec v2.9.1 (!132)
  - Notable Changes
    - Fix the SBOM generation step in the release action
    - Phase out support for go version 1.15 because current ginko is not backward compatible

## v3.3.3
- fix: Return non-zero exit codes when executed through entrypoint script (!131)

## v3.3.2
- chore: Use ruleset.ProcessPassthrough helper (!129)

## v3.3.1
- update tracking calculator (!127)

## v3.3.0
- Use vulninfo instead of cwe-info (!113)

## v3.2.1
- Upgrading dependencies to non-vulnerable versions (!124)

## v3.2.0
- Update golang base image to 1.16 (!100)
- Limit `GOPATH` shimming to only projects without go modules (!100)

## v3.1.3
- Update to gosec v2.8.1 (!119)
  - Minor fixes and updated dependencies
  - Hash.Write whitelisted as it will never return an error that needs to be handled

## v3.1.2
- Update tracking calculator (!123)

## v3.1.1
- Always log errors on failure to fetch dependencies (!114)

## v3.1.0
- Add `COMPILE` variable to support skipping dependency fetching when desired (!111)
- Add `GOSEC_GO_PKG_PATH` variable to give the option to set where go builds the app (!111)
- Update dependency fetching to only download packages and not build/install by default (!111)

## v3.0.1
- Bump `MAJOR` in .gitlab-ci.yml to 3 (!109)

## v3.0.0
- Remove SAST_GOSEC_CONFIG support (!106)

## v2.22.0
- Update to gosec v2.8.0 (!108)
  + None of the updates should have an affect on GitLab users

## v2.21.0
- Integrate tracking-calculator post-analyzer (!99)

## v2.20.1
- Restrict openshift access (!97)

## v2.20.0
- Support openshift (!96)

## v2.19.0
- Update `report` dependency (!92)
  - Update schema version to `14.0.0`

## v2.18.0
- Update gosec to v2.7.0 (!91)
    - Better support for golang 1.16
    - Reset the state of TLS rule after each version check

## v2.17.0
- Add support for multiline vulnerabilities (!90)

## v2.16.3
- Update gosec to v2.6.1 (!87)
  - Updates dependencies
  - Fixes a few typos and errors

## v2.16.2
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!84)

## v2.16.1
- Update golang/x/crypto package to v0.0.0-20201216223049-8b5274cf687f (!83)

## v2.16.0
- Include RawSourceCodeExtract in report (!81)

## v2.15.0
- Update common to v2.22.0 (!82)
- Update urfave/cli to v2.3.0 (!82)

## v2.14.0
- Update gosec to 2.5.0; logrus, cli, and crypto golang dependencies to latest (!80)

## v2.13.2
- Update common to v2.21.3 (!79)

## v2.13.1
- Update common to v2.19.1 (!69)

## v2.13.0
- Add custom rulesets (!63)

## v2.12.3
- Update golang dependencies (!68)

## v2.12.2
- Add link to documentation for rule G304 (!66)

## v2.12.1
- Version bump of cwe-info-go to 1.0.2 which includes the missing information for CWE-118 (!67)

## v2.12.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!62)

## v2.11.0
- Use gosec v2.4.0 (!57)

## v2.10.1
- Upgrade go to version 1.15 (!58)

## v2.10.0
- Add scan object to report (!54)

## v2.9.0
- Switch to the MIT Expat license (!49)

## v2.8.4
- Use gosec v2.3.0 (!50)

## v2.8.3
- Update Debug output to give a better description of command that was ran (!48)

## v2.8.2
- Update common to provide more info-level logging (!47)

## v2.8.1
- Update common version to latest (!45)
- Check gosec exit status and exit with error if not 0 or 1 (!45)

## v2.8.0
- Update Docker image to use golang 1.14 (!46)

## v2.7.1
 - Change GoSec external configuration reading from os.env to cli flag (!43)

## v2.7.0
- Update logging to use commonutil (!42)

## v2.6.2
- Drop securego base docker image in favor of packaging gosec binary (!39)

## v2.6.1
- Remove `location.dependency` from the generated SAST report (!41)

## v2.6.0
- Add support for specifying a `gosec` configuration file with `SAST_GOSEC_CONFIG` (!19 @firelizzard)

## v2.5.1
- Use Alpine as builder image (!33)

## v2.5.0
- Change location where custom CA certs are written (!30)

## v2.4.0
- Add `id` field to vulnerabilities in JSON report (!31)

## v2.3.0
- Add support for custom CA certs (!28)

## v2.2.1
- Use gosec v2.2.0
- Use CWE mappings introduced in gosec v2.2.0
- Change `compareKey` from `<file>:<code>:G-<gosec-rule-id>` to `<file>:<lineno>:<code>:CWE-<cweid>`

## v2.2.0
- Build Docker image on top of securego/gosec:v2.1.0 (!21 @bartjkdp)

## v2.1.1
- Add rule URLs for G101, G102, G103, G104, G107, G201, & G202

## v2.1.0
- Build Docker image on top of securego/gosec:2.0.0 (!16 @bartjkdp)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.5.0 (unreleased)
- Build Docker image on top of securego/gosec:1.2.0

## v1.4.0
- Add `Scanner` property and deprecate `Tool`

## v1.3.0
- Rename this analyzer to gosec from Go AST Scanner (https://gitlab.com/gitlab-org/gitlab-ee/issues/6999)

## v1.2.0
- Show command error output

## v1.1.0
- Enrich report with more data

## v1.0.0
- Rewrite using Go and analyzers common library

## v0.1.0
- initial release
